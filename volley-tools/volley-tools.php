<?php
/*
Plugin Name: Volley Tools
Plugin URI: http://страница_с_описанием_плагина_и_его_обновлений
Description: Краткое описание плагина.
Version: 0.1
Author: CubixSystem
Author URI: http://страница_автора_плагина
*/

/*  Copyright 2014  CubixSystem  (email: cubix.system78@gmail.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// TODO: Убрать дебаг
 ini_set('display_errors', '1');
 ini_set('error_reporting', E_ALL);

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

if (!class_exists('VolleyTools')) {

	class VolleyTools {

		// Хранение внутренних данных
		public $data = array();

		/**
		 * Конструктор объекта
			Инициализация основных переменных
		 */
		function VolleyTools()
		{

			// Объявляем константу инициализации нашего плагина
			DEFINE('VolleyTools', true);

			// Название файла нашего плагина
			$this->plugin_name = plugin_basename(__FILE__);

			// URL адресс для нашего плагина
			$this->plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));

			// Таблицы для хранения
			// обязательно должна быть глобально объявлена перменная $wpdb
			global $wpdb;
			$this->tbl_volley_team   = $wpdb->prefix . 'volley_team';
			$this->tbl_volley_game   = $wpdb->prefix . 'volley_game';
			$this->tbl_volley_place   = $wpdb->prefix . 'volley_place';
			$this->tbl_volley_day   = $wpdb->prefix . 'volley_day';

			 // TODO: Нужна ли установка?
	//		// Функция которая исполняется при установке плагина
	//		register_activation_hook( $this->plugin_name, array(&$this, 'install') );

			// Функция которая исполняется при активации плагина
			register_activation_hook( $this->plugin_name, array(&$this, 'activate') );

			// Функция которая исполняется при деактивации плагина
			register_deactivation_hook( $this->plugin_name, array(&$this, 'deactivate') );

			//  Функция которая исполняется удалении плагина
			register_uninstall_hook( $this->plugin_name, array('VolleyTools', 'uninstall') );

			// Если мы в адм. интерфейсе
			if ( is_admin() ) {

				// Добавляем стили и скрипты
				// NOTE: Скрипты для админки
				// TODO: Проследить подключение стилей и скриптов при выключенном плагине
				add_action('admin_enqueue_scripts', array(&$this, 'admin_load_scripts'));
				add_action('admin_enqueue_scripts', array(&$this, 'admin_load_styles'));
				// Добавляем меню для плагина
				add_action( 'admin_menu', array(&$this, 'admin_generate_menu') );

				include_once('includes/volley_day/volley_day.php');

				volley_day_register_hooks();

			} else {
				// Добавляем стили и скрипты
	//			add_action('wp_print_scripts', array(&$this, 'site_load_scripts'));
	//			add_action('wp_print_styles', array(&$this, 'site_load_styles'));

				// TODO: Добавить шорткод
	//			add_shortcode('show_reviews', array (&$this, 'site_show_reviews'));
			}
		}

		/**
		 * Загрузка необходимых скриптов для страницы управления
		 * в панели администрирования
		 */
		function admin_load_scripts()
		{
	//		// Регистрируем скрипты
			volley_day_register_scripts($this->plugin_url);
		}

		/**
		 * Загрузка необходимых стилей для страницы управления
		 * в панели администрирования
		 */
		function admin_load_styles()
		{
			// Регистрируем стили
			wp_register_style('volleyToolsAdminCss', $this->plugin_url . 'assets/css/admin-style.css' );
			wp_register_style('jquery_ui_css', $this->plugin_url . 'assets/jquery-ui/jquery-ui.min.css' );
		}

		/**
		 * Генерируем меню
		 */
		function admin_generate_menu()
		{
			// Добавляем основной раздел меню
			add_menu_page('Добро пожаловать в модуль управления играми', 'Игры', 'edit_published_pages', 'volley-tools', array(&$this, 'admin_volley_games'));
			// Добавляем дополнительный разделы
			add_submenu_page( 'volley-tools', 'Управление командами', 'Команды', 'edit_published_pages', 'volley_teams', array(&$this,'admin_volley_teams'));
			add_submenu_page( 'volley-tools', 'Управление местами', 'Места', 'edit_published_pages', 'volley_places', array(&$this,'admin_volley_places'));
			add_submenu_page( 'volley-tools', 'Управление днями', 'Дни', 'edit_published_pages', 'volley_days', array(&$this,'admin_volley_days'));
			add_submenu_page( 'volley-tools', 'Управление содержимым', 'О плагине', 'edit_published_pages', 'plugin_info', array(&$this,'admin_plugin_info'));

		}

		/**
		 * [[Description]]
		 */
		function admin_volley_games()
		{
			wp_enqueue_style('volleyToolsAdminCss');

			 include_once('includes/volley_game/volley_game.php');
		}

		 /**
		  * [[Description]]
		  */
		 public function admin_volley_teams()
		 {
			 wp_enqueue_style('volleyToolsAdminCss');

			 include_once('includes/volley_team/volley_team.php');
		 }

		 /**
		  * [[Description]]
		  */
		 public function admin_volley_places()
		 {
			 wp_enqueue_style('volleyToolsAdminCss');

			 include_once('includes/volley_place/volley_place.php');
		 }

		 /**
		  * [[Description]]
		  */
		 public function admin_volley_days()
		 {
			 volley_day_load($this->tbl_volley_day);
		 }

		/**
		 * [[Description]]
		 */
		public function admin_plugin_info()
		{
			wp_enqueue_style('volleyToolsAdminCss');

			include_once('includes/plugin_info/plugin_info.php');
		}

		/**
		 * [[Description]]
		 */
		function site_load_styles()
		{
	//		// Регистрируем стили
	//		wp_register_style('volleyToolsSiteCss', $this->plugin_url . 'assets/css/site-style.css' );
	//		// Добавляем стили
	//		wp_enqueue_style('volleyToolsSiteCss');
		}

		 function site_load_scripts()
		{

		}

		/**
		 * Активация плагина
		 * @returns Boolean [[Description]]
		 */
		function activate()
		{
			global $wpdb;

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

			$table_game = $this->tbl_volley_game;
			$table_team = $this->tbl_volley_team;
			$table_place = $this->tbl_volley_place;
			$table_day = $this->tbl_volley_day;

			//TODO: Определение версии mysql
	//		if ( version_compare(mysql_get_server_info(), '4.1.0', '>=') ) {
	//			if ( ! empty($wpdb->charset) )
	//				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
	//			if ( ! empty($wpdb->collate) )
	//				$charset_collate .= " COLLATE $wpdb->collate";
	//		}

			// Структуры наших таблиц
			$sql_table_team = "
					CREATE TABLE `".$wpdb->prefix."volley_team` (
						id MEDIUMINT( 9 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
						team_name VARCHAR( 254 ) NOT NULL ,
						team_city VARCHAR( 254 ) NOT NULL ,
						UNIQUE KEY id (id)
					)".$charset_collate.";";

			$sql_table_game = "
					CREATE TABLE `".$wpdb->prefix."volley_game` (
						id MEDIUMINT( 9 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
						team1_name VARCHAR( 254 ) NOT NULL ,
						team2_name VARCHAR( 254 ) NOT NULL ,
						team1_score TINYINT( 2 ) UNSIGNED NULL ,
						team2_score TINYINT( 2 ) UNSIGNED NULL ,
						game_date DATE NOT NULL ,
						game_time TIME NOT NULL ,
						game_place VARCHAR( 254 ) NOT NULL ,
						game_exist BIT NULL,
						UNIQUE KEY id (id)
					)".$charset_collate.";";

			$sql_table_place = "
				CREATE TABLE `".$wpdb->prefix."volley_place` (
					id MEDIUMINT( 9 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
					game_place VARCHAR( 254 ) NOT NULL ,
					CONSTRAINT game_place UNIQUE (game_place),
					UNIQUE KEY id (id)
				)".$charset_collate.";";

			$sql_table_day = "
				CREATE TABLE `".$wpdb->prefix."volley_day` (
					id MEDIUMINT( 9 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
					game_count MEDIUMINT( 9 ) UNSIGNED NOT NULL ,
					game_date DATE NOT NULL ,
					CONSTRAINT game_date UNIQUE (game_date),
					UNIQUE KEY id (id)
				)".$charset_collate.";";

			//Проверка на существование таблиц
			if ( $wpdb->get_var("show tables like '".$table_game."'") != $table_game ) {
				dbDelta($sql_table_game);
			}

			if ( $wpdb->get_var("show tables like '".$table_team."'") != $table_team ) {
				dbDelta($sql_table_team);
			}

			if ( $wpdb->get_var("show tables like '".$table_place."'") != $table_place ) {
				dbDelta($sql_table_place);
			}

			if ( $wpdb->get_var("show tables like '".$table_day."'") != $table_day ) {
				dbDelta($sql_table_day);
			}

		}

		/**
		 * [[Description]]
		 * @returns Boolean [[Description]]
		 */
		function deactivate()
		{
			return true;
		}

		/**
		 *  Удаление плагина
		 */
		public static function uninstall()
		{
			global $wpdb;

			$table_game = $wpdb->prefix . 'volley_game';
			$table_team = $wpdb->prefix . 'volley_team';
			$table_place = $wpdb->prefix . 'volley_place';
			$table_day = $wpdb->prefix . 'volley_day';

			$wpdb->query("DROP TABLE IF EXISTS {$table_game}");
			$wpdb->query("DROP TABLE IF EXISTS {$table_team}");
			$wpdb->query("DROP TABLE IF EXISTS {$table_place}");
			$wpdb->query("DROP TABLE IF EXISTS {$table_day}");
		}
	}
}

global $VolleyTools;
$VolleyTools = new VolleyTools();
?>
