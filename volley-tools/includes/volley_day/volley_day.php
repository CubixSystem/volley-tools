<?php
/**
 * [[Description]]
 * @param   [[Type]] $tbl_volley_day [[Description]]
 * @returns Boolean  [[Description]]
 */
function  volley_day_get_days($tbl_volley_day)
{

	global $wpdb;

	$sql="SELECT * FROM " .$tbl_volley_day." ORDER BY game_date ASC";
	$results[0] = $wpdb->get_results($sql, ARRAY_A);

	$sql="SELECT COUNT(*) FROM " .$tbl_volley_day;
	$results[1] = $wpdb->get_results($sql, ARRAY_A);

	$sql="SELECT SUM(`game_count`) FROM " .$tbl_volley_day;
	$results[2] = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Записей в БД нет</b></div>
	<?php
		return false;
	}
	 return $results;
}

/**
 * proccesses actions (delete / modify )
 * @param [[Type]] $tbl_volley_day [[Description]]
 */
function  volley_day_proccess_form($tbl_volley_day)
{

	global $wpdb;

	if(isset($_REQUEST['button_new']))
	{
		volley_day_add_day($tbl_volley_day);
	}

	if(isset($_REQUEST['button_renew']))
	{
		volley_day_delete_days($tbl_volley_day);
		volley_day_modify_days($tbl_volley_day);
	}
}

/**
 * adds day
 * @param   [[Type]] $tbl_volley_day [[Description]]
 * @returns [[Type]] [[Description]]
 */
function volley_day_add_day($tbl_volley_day)
{
	global $wpdb;

	if (isset($_REQUEST['new_game_day'])) $game_day = $_REQUEST['new_game_day'];
	if (isset($_REQUEST['new_game_day'])) $game_count = $_REQUEST['new_game_count'];

	if (!empty($game_day))
	{
		$game_day  = esc_sql($game_day);
		$game_day  = esc_sql($game_day);

		$sql = 'INSERT INTO '.$tbl_volley_day.' (game_date, game_count) VALUES ("'.$game_day.'", "'.$game_count.'")';
		$wpdb->query($sql);
	}
}

/**
 * [[Description]]
 * @param [[Type]] $tbl_volley_day [[Description]]
 */
function volley_day_delete_days($tbl_volley_day)
{
	global $wpdb;

	if (isset($_REQUEST['del'])) $dels   = $_REQUEST['del'];

	if (!empty($dels))
	{
		foreach ($dels as $id)
		{
			$sql = 'DELETE FROM '.$tbl_volley_day.' WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

/**
 * [[Description]]
 * @param [[Type]] $tbl_volley_day [[Description]]
 */
function volley_day_modify_days()
{
	global $wpdb;
	global $VolleyTools;

	$game_counts = $_POST['game_counts'];
	$game_days  = $_REQUEST['game_days'];
	$game_ids  = $_REQUEST['game_ids'];

	foreach ($game_days as $i=>$game_day)
	{
		if (!empty($game_day)) $game_day  = esc_sql($game_day);
		if (!empty($game_counts[$i])) $game_count  = esc_sql($game_counts[$i]);
		if (!empty($game_ids[$i])) $game_id  = esc_sql($game_ids[$i]);

		$sql ='UPDATE '.$VolleyTools->tbl_volley_day.' SET game_date="'.$game_day.'", game_count="'.$game_count.'" WHERE id='.$game_id;
		$wpdb->query($sql);
	}
}

/**
 * [[Description]]
 * @param [[Type]] $days [[Description]]
 */
function volley_day_show_days($days)
{
	 include_once('volley_day_template.php');
}

/**
 * [[Description]]
 * @param [[Type]] $tbl_volley_day [[Description]]
 */
function volley_day_load($tbl_volley_day)
{
	 wp_enqueue_style('volleyToolsAdminCss');
	 wp_enqueue_style('jquery_ui_css');

	 wp_enqueue_script( 'jquery' );
	 wp_enqueue_script( 'jquery-ui-datepicker' );
	 wp_enqueue_script('volley_day');
	 wp_enqueue_script('volley_day_ajax');

 	$days =  volley_day_get_days($tbl_volley_day);
 	volley_day_show_days($days);
}

/**
 * [[Description]]
 * @param [[Type]] $plugin_url [[Description]]
 */
function volley_day_register_scripts($plugin_url)
{
	wp_register_script('volley_day', $plugin_url . 'includes/volley_day/js/volley_day.js' );
	wp_register_script('volley_day_ajax', $plugin_url . 'includes/volley_day/js/volley_day_ajax.js' );

	wp_localize_script( 'jquery', 'volley_day',
	array(
		'url' => admin_url('admin-ajax.php'),
		'nonce' => wp_create_nonce('volley_day_ajax_nonce')
	));
}

function volley_day_register_hooks()
{
	add_action( 'wp_ajax_volley_day_ajax', 'volley_day_ajax' );
}

function volley_day_ajax() {
	$nonce = $_POST['nonce'];

	// проверяем nonce код, если проверка не пройдена прерываем обработку
	if ( !wp_verify_nonce( $nonce, 'volley_day_ajax_nonce' ) )
		die ( 'Stop!');

	// обрабатываем данные и возвращаем
	volley_day_modify_days();


	// Не забываем выходить
	exit;
}

volley_day_register_hooks();

?>
