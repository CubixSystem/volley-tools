"use strict";

function send_ajax(formid)
{
	var form = jq(formid);
	var days = jQuery('[name^="game_day"]');
	var counts = jQuery('[name^="game_count"]');

	var game_ids = days.map(function () {
		return this.name.match(/\d+/);
	}).toArray();

	var game_days = days.map(function () {
		return this.value;
	}).toArray();

	var game_counts = counts.map(function () {
		return this.value;
	}).toArray();

//	console.log(game_ids);
//	console.log(game_days);
//	console.log(game_counts);

	jQuery.post(
		volley_day.url,
		{
		action : 'volley_day_ajax',

		game_ids : game_ids,
		game_days : game_days,
		game_counts : game_counts,

		// отправим код nonce вместе с остальными данными
		nonce : volley_day.nonce
		},
		function( response ) {
//			alert( response );
			console.log(response);
		}
	);
}
