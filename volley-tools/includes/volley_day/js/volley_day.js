"use strict";

function jq(myid) {
	return "#" + myid.replace(/(:|\.|\[|\])/g, "\\$1");
}

function call_calendar() {
	jQuery(function () {
		jQuery(".datepicker").datepicker({
			showWeek: false,
			dateFormat: "yy-mm-dd",
			showButtonPanel: true,
			firstDay: 1
		});
	});
}

call_calendar();
