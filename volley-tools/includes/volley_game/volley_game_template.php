<div class="wrap">

	<h2>Игры</h2>

	<table>

		<?php if (!empty($games)) { ?>

		<form action="" method="post">
			<tr>
				<th><b>ID</b>
				</th>
				<th><b>Команда 1</b>
				</th>
				<th><b>Счет<br>команды 1</b>
				</th>
				<th><b>Команда 2</b>
				</th>
				<th><b>Счет<br>команды 2</b>
				</th>
				<th><b>Дата игры</b>
				</th>
				<th><b>Время игры</b>
				</th>
				<th><b>Место игры</b>
				</th>
				<th><b>Игра<br>состоялась</b>
				</th>
				<th>&nbsp;
				</th>
				<th><b>Удалить</b>
				</th>
			</tr>

			<?php foreach ($games as $game) { ?>
			<tr>
				<td>
					<?php echo $game[ 'id']?>
				</td>
				<td>
					<select selected="<?php echo $game['team1_name']?>" name="team1_name[<?php echo $team1_name['id'] ?>]" >
					<?php foreach ($teams as $team) { ?>
						<option value="<?php echo $team['team_name'] ?>"><?php echo $team['team_name'] ?></option>
					<?php } ?>
					</select>
				</td>
				<td>
					<input  type="number" min="0" name="team1_score[<?php echo $team1_score['id']?>]" value="<?php echo $game['team1_score']?>" />
				</td>
				<td>
					<select selected="<?php echo $game['team2_name']?>" name="team2_name[<?php echo $team2_name['id'] ?>]">
						<?php foreach ($teams as $team) { ?>
						<option value="<?php echo $team['team_name'] ?>">
							<?php echo $team[ 'team_name'] ?>
						</option>
						<?php } ?>
					</select>
				</td>
				<td>
					<input  type="number" min="0" name="team2_score[<?php echo $team2_score['id']?>]" value="<?php echo $game['team2_score']?>" />
				</td>
				<td>
					<input type="text" name="game_date[<?php echo $game_date['id'] ?>]" value="<?php echo $game['game_date'] ?>" />
				</td>
				<td>
					<input type="time" name="game_time[<?php echo $game_time['id']?>]" value="<?php echo $game['game_time']?>" />
				</td>
				<td>
					<select selected="<?php echo $game['game_place']?>" name="game_place[<?php echo $game_place['id'] ?>]">
						<?php foreach ($places as $place) { ?>
						<option value="<?php echo $place['game_place'] ?>">
							<?php echo $place[ 'game_place'] ?>
						</option>
						<?php } ?>
					</select>
				</td>
				<td>
					<input type="checkbox" name="game_exist[<?php echo $game_exist['id']?>]" value="<?php echo $game['game_exist'] ?>"
					  <?php if($game['game_exist']==1) echo("checked=\"checked\"") ?> />
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					<input type="checkbox" name="del[<?php echo $game['id']?>]" value="<?php echo $game['id']?>" />
				</td>
			</tr>
			<?php } ?>

			<tr>
				<th><b>ID</b>
				</th>
				<th><b>Команда 1</b>
				</th>
				<th><b>Счет<br>команды 1</b>
				</th>
				<th><b>Команда 2</b>
				</th>
				<th><b>Счет<br>команды 2</b>
				</th>
				<th><b>Дата игры</b>
				</th>
				<th><b>Время игры</b>
				</th>
				<th><b>Место игры</b>
				</th>
				<th><b>Игра<br>состоялась</b>
				</th>
				<th>&nbsp;
				</th>
				<th><b>Удалить</b>
				</th>
			</tr>

		<tr><td>&nbsp;</td></tr>

			<tr>
				<td>
					<input type="submit" class="button action" value="Обновить" name="button_renew" />
				</td>
			</tr>
		</form>

		<tr>
			<td>&nbsp;</td>
		</tr>
		<?php }?>


<!--//NEW-->
		<form action="" method="post">
			<tr>
				<th>&nbsp;
				</th>
				<th><b>Команда 1</b>
				</th>
				<th><b>Счет<br>команды 1</b>
				</th>
				<th><b>Команда 2</b>
				</th>
				<th><b>Счет<br>команды 2</b>
				</th>
				<th><b>Дата игры</b>
				</th>
				<th><b>Время игры</b>
				</th>
				<th><b>Место игры</b>
				</th>
				<th><b>Игра<br>состоялась</b>
				</th>
			</tr>
			<tr>
				<td>
					<input type="submit" class="button button-primary" value="Добавить" name="button_new" />
				</td>
				<td>
<!--				//TODO:Одинаковые имена команд
				-->
					<select name="new_team1_name">
						<option></option>
						<?php foreach ($teams as $team) { ?>
						<option value="<?php echo $team['team_name'] ?>">
							<?php echo $team[ 'team_name'] ?>
						</option>
						<?php } ?>
					</select>

<!--					<input type="text" name="new_team1_name" />-->
				</td>
				<td>
					<input  type="number" min="0" name="new_team1_score" />
				</td>
				<td>
<!--				//TODO:Одинаковые имена команд
				-->
					<select name="new_team2_name">
						<option></option>
						<?php foreach ($teams as $team) { ?>
						<option value="<?php echo $team['team_name'] ?>">
							<?php echo $team[ 'team_name'] ?>
						</option>
						<?php } ?>
					</select>
				</td>
				<td>
					<input  type="number" min="0" name="new_team2_score" />
				</td>
				<td>
					<input type="text" name="new_game_date" />
				</td>
				<td>
					<input type="time" name="new_game_time" />
				</td>
				<td>
					<select name="new_game_place">
						<option></option>
						<?php foreach ($places as $place) { ?>
						<option value="<?php echo $place['game_place'] ?>">
							<?php echo $place[ 'game_place'] ?>
						</option>
						<?php } ?>
					</select>
				</td>
				<td>
					<input type="checkbox" name="new_game_exist" />
				</td>
				<td>&nbsp;</td>
			</tr>

		</form>

	</table>
</div>
