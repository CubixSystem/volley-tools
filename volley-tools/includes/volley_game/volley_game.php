<?php
function get_games($tbl_volley_game)
{

	global $wpdb;

	$sql="SELECT * FROM " .$tbl_volley_game." ORDER BY game_date ASC";

	$results = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Игр в БД нет</b></div>
	<?php
		return false;
	}

	 return $results;
}

function get_teams($tbl_volley_team)
{

	global $wpdb;

	$sql="SELECT team_name FROM " .$tbl_volley_team." ORDER BY team_name ASC";

	$results = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Команд в БД нет</b></div>
	<?php
		return false;
	}

	 return $results;
}

function get_places($tbl_volley_place)
{

	global $wpdb;

	$sql="SELECT game_place FROM " .$tbl_volley_place." ORDER BY game_place ASC";

	$results = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Записей о местах в БД нет</b></div>
	<?php
		return false;
	}

	 return $results;
}

// proccesses actions (delete / modify )

function proccess_games_form($tbl_volley_game)
{

	global $wpdb;

	if(isset($_REQUEST['button_new']))
	{
		add_game($tbl_volley_game);
	}

	if(isset($_REQUEST['button_renew']))
	{
		delete_games($tbl_volley_game);
		modify_games($tbl_volley_game);
	}

	//TODO: Работа с пустыми полями
//	if (isset($_REQUEST['new_game_exist'])) $new_game_exist  = $_REQUEST['new_game_exist'];
//	else $new_game_exist = 0;
//
//	if (!empty($_REQUEST['new_team1_name']))
//	{
//		add_game($_REQUEST['new_team1_name'], $_REQUEST['new_team2_name'],
//				 $_REQUEST['new_team1_score'], $_REQUEST['new_team2_score'],
//				  $_REQUEST['new_game_date'], $_REQUEST['new_game_time'],
//				  $_REQUEST['new_game_place'],
//				$new_game_exist,
//				 $tbl_volley_game);
//	}
}

// adds game

function add_game($tbl_volley_game)
{
	global $wpdb;

	if (!empty($_REQUEST['new_team1_name'])) $team1_name  = esc_sql($_REQUEST['new_team1_name']); else return -1;
	if (!empty($_REQUEST['new_team2_name'])) $team2_name = esc_sql($_REQUEST['new_team2_name']); else  return -1;
	if (!empty($_REQUEST['new_team1_score'])) $team1_score  = esc_sql($_REQUEST['new_team1_score']); else return -1;
	if (!empty($_REQUEST['new_team2_score'])) $team2_score = esc_sql($_REQUEST['new_team2_score']); else  return -1;
	if (!empty($_REQUEST['new_game_date'])) $game_date  = esc_sql($_REQUEST['new_game_date']); else return -1;
	if (!empty($_REQUEST['new_game_time'])) $game_time = esc_sql($_REQUEST['new_game_time']); else  return -1;
	if (!empty($_REQUEST['new_game_place'])) $game_place = esc_sql($_REQUEST['new_game_place']); else  return -1;
	if (!empty($_REQUEST['new_game_exist'])) $game_exist = 1; else  $game_exist = 0;

	$sql = 'INSERT INTO '.$tbl_volley_game.' (team1_name, team2_name, team1_score, team2_score, game_date, game_time, game_place, game_exist) VALUES ("'.$team1_name.'", "'.$team2_name.'", "'.$team1_score.'", "'.$team2_score.'", "'.$game_date.'", "'.$game_time.'", "'.$game_place.'", "'.$game_exist.'")';
	$wpdb->query($sql);

}

function delete_games($tbl_volley_game)
{
	global $wpdb;

	if (isset($_REQUEST['del'])) $dels   = $_REQUEST['del'];

	if (!empty($dels))
	{
		foreach ($dels as $id)
		{
			$sql = 'DELETE FROM '.$tbl_volley_game.' WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function modify_games($tbl_volley_game)
{
	if (isset($_REQUEST['team1_name'])) $team1_names  = $_REQUEST['team1_name'];
	if (isset($_REQUEST['team2_name'])) $team2_names = $_REQUEST['team2_name'];
	if (isset($_REQUEST['team1_score'])) $team1_scores  = $_REQUEST['team1_score'];
	if (isset($_REQUEST['team2_score'])) $team2_scores = $_REQUEST['team2_score'];
	if (isset($_REQUEST['game_date'])) $game_dates  = $_REQUEST['game_date'];
	if (isset($_REQUEST['game_time'])) $game_times = $_REQUEST['game_time'];
	if (isset($_REQUEST['game_place'])) $game_places  = $_REQUEST['game_place'];
	if (isset($_REQUEST['game_exist'])) $game_exists  = $_REQUEST['game_exist'];

	var_dump($team1_names);

	//TODO: Проверки, проверки и еще раз проверки!)
	if ( (!empty($team1_names)) && (is_array($team1_names)))
	{

		foreach ($team1_names as $id=>$team1_name)
		{

			if (!empty($team1_name)) $team1_name  = esc_sql($team1_name); else {echo("1"); break;}
			if (!empty($team2_names[$id])) $team2_name = esc_sql($team2_names[$id]); else {echo("2"); break;}
			if (!empty($team1_scores[$id])) $team1_score = esc_sql($team1_scores[$id]); else {echo("3"); break;}
			if (!empty($team2_scores[$id])) $team2_score = esc_sql($team2_scores[$id]); else {echo("4"); break;}
			if (!empty($game_dates[$id])) $game_date = esc_sql($game_dates[$id]); else {echo("5"); break;}
			if (!empty($game_times[$id])) $game_time = esc_sql($game_times[$id]); else {echo("1"); break;}
			if (!empty($game_places[$id])) $game_place = esc_sql($game_places[$id]); else {echo("1"); break;}
//			if (!empty($game_exists[$id])) $game_exist = esc_sql($game_exists[$id]); else break;
			$game_exist = esc_sql($game_exists[$id]);

			$sql ='UPDATE '.$tbl_volley_game.' SET team1_name="'.$team1_name.'", team2_name="'.$team2_name.'", team1_score="'.$team1_score.'", team2_score="'.$team2_score.'", game_date="'.$game_date.'", game_time="'.$game_time.'", game_place="'.$game_place.'", $game_exist="'.$game_exist.'" WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function show_games($games,$teams,$places)
{
	 include_once('volley_game_template.php');
}

$teams = get_teams($this->tbl_volley_team);
$places = get_places($this->tbl_volley_place);

proccess_games_form($this->tbl_volley_game);
$games = get_games($this->tbl_volley_game);

show_games($games,$teams,$places);
?>



