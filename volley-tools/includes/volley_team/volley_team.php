<?php
function get_teams($tbl_volley_team)
{

	global $wpdb;

	$sql="SELECT * FROM " .$tbl_volley_team." ORDER BY team_name ASC";

	$results = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Команд в БД нет</b></div>
	<?php
		return false;
	}

	 return $results;
}

// proccesses actions (delete / modify )

function proccess_teams_form($tbl_volley_team)
{

	global $wpdb;

	if(isset($_REQUEST['button_new']))
	{
		add_team($tbl_volley_team);
	}

	if(isset($_REQUEST['button_renew']))
	{
		delete_teams($tbl_volley_team);
		modify_teams($tbl_volley_team);
	}
}

// adds team

function add_team($tbl_volley_team)
{
	$team_name = $_REQUEST['new_team_name'];
	$team_city = $_REQUEST['new_team_city'];

	global $wpdb;

	if (!empty($team_name)) $team_name  = esc_sql($team_name); else return -1;
	if (!empty($team_city)) $team_city = esc_sql($team_city); else  return -1;

	$sql = 'INSERT INTO '.$tbl_volley_team.' (team_name, team_city) VALUES ("'.$team_name.'", "'.$team_city.'")';
	$wpdb->query($sql);

}

function delete_teams($tbl_volley_team)
{
	global $wpdb;

	if (isset($_REQUEST['del'])) $dels   = $_REQUEST['del'];

	if (!empty($dels))
	{
		foreach ($dels as $id)
		{
			$sql = 'DELETE FROM '.$tbl_volley_team.' WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function modify_teams($tbl_volley_team)
{
	global $wpdb;

	if (isset($_REQUEST['team_name'])) $team_names  = $_REQUEST['team_name'];
	if (isset($_REQUEST['team_city'])) $team_cities = $_REQUEST['team_city'];

	if ( (!empty($team_names)) && (is_array($team_names)))
	{

		foreach ($team_names as $id=>$team_name)
		{

			if (!empty($team_name)) $team_name  = esc_sql($team_name); else break;
			if (!empty($team_cities[$id])) $team_city = esc_sql($team_cities[$id]); else break;

			$sql ='UPDATE '.$tbl_volley_team.' SET team_name="'.$team_name.'", team_city="'.$team_city.'" WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function show_teams($teams)
{
	 include_once('volley_team_template.php');
}

 proccess_teams_form($this->tbl_volley_team);
 $teams = get_teams($this->tbl_volley_team);
 show_teams($teams);
?>



