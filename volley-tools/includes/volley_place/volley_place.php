<?php
function get_places($tbl_volley_place)
{

	global $wpdb;

	$sql="SELECT * FROM " .$tbl_volley_place." ORDER BY game_place ASC";

	$results = $wpdb->get_results($sql, ARRAY_A);

	if (empty($results))
	{
	?>
		<div class="updated"><b>Записей в БД нет</b></div>
	<?php
		return false;
	}

	 return $results;
}

// proccesses actions (delete / modify )

function proccess_places_form($tbl_volley_place)
{

	global $wpdb;

	if(isset($_REQUEST['button_new']))
	{
		add_place($tbl_volley_place);
	}

	if(isset($_REQUEST['button_renew']))
	{
		delete_places($tbl_volley_place);
		modify_places($tbl_volley_place);
	}
}

// adds place

function add_place($tbl_volley_place)
{
	$game_place = $_REQUEST['new_game_place'];

	global $wpdb;

	if (!empty($game_place)) $game_place  = esc_sql($game_place); else return -1;

	$sql = 'INSERT INTO '.$tbl_volley_place.' (game_place) VALUES ("'.$game_place.'")';
	$wpdb->query($sql);

}

function delete_places($tbl_volley_place)
{
	global $wpdb;

	if (isset($_REQUEST['del'])) $dels   = $_REQUEST['del'];

	if (!empty($dels))
	{
		foreach ($dels as $id)
		{
			$sql = 'DELETE FROM '.$tbl_volley_place.' WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function modify_places($tbl_volley_place)
{
	global $wpdb;

	if (isset($_REQUEST['game_place'])) $game_places  = $_REQUEST['game_place'];
	if (isset($_REQUEST['enabled'])) $enabled   = $_REQUEST['enabled'];


	if (!empty($enabled))
	{
		foreach ($game_places as $id=>$game_place)
		{
			$game_place  = esc_sql($game_place);
			$sql ='UPDATE '.$tbl_volley_place.' SET game_place="'.$game_place.'" WHERE id='.$id;
			$wpdb->query($sql);
		}
	}
}

function show_places($places)
{
	 include_once('volley_place_template.php');
}

 proccess_places_form($this->tbl_volley_place);
 $places = get_places($this->tbl_volley_place);
 show_places($places);
?>



